#!/bin/bash

#$ -S /bin/sh
# This script automatically run CPU benchmarking.

echo "CS553 Assignment 1 CPU Benchmarking"
echo "    Designed by Lu Wang (A20315534)"

# int operations test, get IOPS
for num_threads in 1 2 4 8 16
do
	for n in 1 2 3
	do
		echo attempt $n
		bin/cpu_linux -int -threads $num_threads -loops 1000000000
	done
done

# float operations test, get FLOPS
for num_threads in 1 2 4 8 16
do
	for n in 1 2 3
	do
		echo attempt $n
		bin/cpu_linux -float -threads $num_threads -loops 1000000000
	done
done
