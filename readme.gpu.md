## Compile

`nvcc -o gpu_linux gpu.cpp`

## Basic usage

assume filename is `gpu_linux`.

`gpu_linux [-threads NUM] [-loops NUM] [-int|-float] [-memtest] [-blocksize] [-h]`

## Help code

```
CS553 Assignment 1 - GPU Benchmarking

Arguments:

-loops: number of loops does the benchmarking do.
        default: 10000000

-float: do float benchmarking.

-int: do int benchmarking.

-threads: number of threads

-memtest: do memory test instead

-blocksize: the size used in memory test (byte, kb, mb)

-h: show this screen.
```

## Example Usage:

### Do GPU compute test, default variables

`./gpu_linux`

### Do Graphic memory test, default variables

`./gpu_linux -memtest`

### Do GPU compute test. Use 32 threads, each threads do 3000 loops and the type is int

`./gpu_linux -threads 32 -loops 3000 -int`

### Do Graphic memory test, with each time it copy 1 kb data.

`./gpu_linux -memtest -blocksize kb`
