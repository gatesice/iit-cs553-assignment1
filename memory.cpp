// =====================================================
//
//   ass1_memory.cpp
//
//   author: Gates_ice
//
//   CS553 Assignment 1
//   Memory Benchmarking section.
//
// =====================================================

#include <iostream>
#include <iomanip>

#include <pthread.h>

#include <chrono>

#include <cstring>
#include <ctime>

using namespace std;

// ### Global variables ### //
enum AccessMode { SEQUENTIAL, RANDOM };

typedef struct {
	int block_size;
	int total_size;
	int access_mode;
	int threads;
	int loops;
	int *timetable;
	char *rbuffer;
	char *wbuffer;
} Global;

Global g;

// ### Thread Functions ### //

/**
 * get a random address
 * this random address should be valid, which can directly used in memtest().
 **/
bool rand_address_warning = true;
int rand_address() {
	// Test if there will be some problem getting random numbers.
	if (RAND_MAX < g.total_size * g.threads && rand_address_warning) {
		cout << "The total memory block is larger than RAND_MAX, there may be some problem getting the random address." << endl;
		rand_address_warning = false;
	}

	// Get a random address.
	int addr = rand() % (g.total_size * g.threads - g.block_size);

	return addr;
}

void *memtest(void *args) {
	// get current tid. If there is 1 thread in all, tid is 0.
	int tid = 0;
	if (g.threads > 1) {
		tid = *((int*) args);
	}

	// begin time measurement.
	auto begin = chrono::high_resolution_clock::now();

	// main loop
	if (AccessMode::SEQUENTIAL == g.access_mode) {
		for (int offset = tid * g.block_size; offset < g.total_size * g.threads; offset += g.block_size * g.threads) {
			memcpy(&(g.wbuffer[offset]), &(g.rbuffer[offset]), g.block_size);
		}
	} else if (AccessMode::RANDOM == g.access_mode) {
		for (int loop = 0; loop < g.loops; loop ++) {
			int offset = rand_address();
			memcpy(&(g.wbuffer[offset]), &(g.rbuffer[offset]), g.block_size);
		}
	}


	// end time measurement
	auto end = chrono::high_resolution_clock::now();
	auto dur = end - begin;
	g.timetable[tid] = std::chrono::duration_cast<std::chrono::milliseconds>(dur).count();

}


// ### Miscellious functions ### //

/**
 * show program help screen.
 */
void show_help() {
	cout << "CS553 Assignment 1 - Memory Benchmarking" << endl << endl

		<< "Arguments:" << endl << endl

		<< setw(8) << "-blocksize: the size each time program read/write." << endl
		<< setw(8) << "            byte|kb|mb|<number in bytes>" << endl
		<< setw(8) << "            byte=1, kb=1024, mb=1024*1024" << endl << endl
		<< setw(8) << "            default: byte" << endl << endl

		<< setw(8) << "-seq -sequential: run this program in sequential access." << endl << endl

		<< setw(8) << "-rnd -random: run this program in random access." << endl << endl

		<< setw(8) << "-loops: number of loops when random access, default=10000" << endl << endl

		<< setw(8) << "-totalsize: total size of memory blocks, per thread." << endl
		<< setw(8) << "            if you have 3 threads and 1000 total size, you will actually have 3 * 1000 = 3000 in total." << endl << endl

		<< setw(8) << "-threads: number of threads." << endl
		<< setw(8) << "          If 1 is specified, program will not use pthread lib." << endl
		<< setw(8) << "          default: 1" << endl << endl;

}

/**
 * show copyright message.
 */
void show_copyright() {
	cout << "CS553 Assignment 1 - Memory Benchmarking" << endl
		<< "    Author: Gates Wong (Lu Wang, A20315534)" << endl
		<< "    use `ass1_cpu -h` to see more helps." << endl;
}

/**
 * show results.
 */
void show_results() {
	     // `={12} RESULTS ={12}`
	cout << "============ RESULTS ============" << endl << endl

		// `Access mode: ((?:sequential)|(?:random))`
		<< "Access mode: " << (g.access_mode == AccessMode::SEQUENTIAL ? "sequential" : "random") << endl

		// `Number of threads: (\d+)`
		<< "Number of threads: " << g.threads << endl;

	if (g.access_mode == AccessMode::RANDOM) {
		// `Number of loops: (\d+)`
		cout << "Number of loops: " << g.loops << endl;
	}

	// List all time.
	for (int ii = 0; ii < g.threads; ii ++) {
		// `thread (\d+) takes (\d+) ms to run.`
		cout << "thread " << ii << " takes " << g.timetable[ii] << " ms to run. " << endl;
	}
}


// ### main() ### //

int main(int argc, char* argv[]) {
	// default values
	g.block_size = 1;
	g.total_size = 100 * 1024 * 1024;
	g.access_mode = AccessMode::SEQUENTIAL;
	g.threads = 1;
	g.loops = 10000;

	// parse the CLI arguments.
	for (int ii = 0; ii < argc; ii ++) {
		if (!strcmp(argv[ii], "-blocksize")) {
			ii ++;
			if (!strcmp(argv[ii], "byte")) {
				g.block_size = 1;
			} else if (!strcmp(argv[ii], "kb")) {
				g.block_size = 1 * 1024;
			} else if (!strcmp(argv[ii], "mb")) {
				g.block_size = 1 * 1024 * 1024;
			} else {
				g.block_size = atoi(argv[ii]);
			}
			continue;
		} else if (!strcmp(argv[ii], "-loops")) {
			g.loops = atoi(argv[++ii]);
			continue;
		} else if (!strcmp(argv[ii], "-totalsize")) {
			g.total_size = atoi(argv[++ii]);
			continue;
		} else if (!strcmp(argv[ii], "-seq") || !strcmp(argv[ii], "-sequential")) {
			g.access_mode = AccessMode::SEQUENTIAL;
			continue;
		} else if (!strcmp(argv[ii], "-rnd") || !strcmp(argv[ii], "-random")){
			g.access_mode = AccessMode::RANDOM;
			continue;
		} else if (!strcmp(argv[ii], "-threads")) {
			g.threads = atoi(argv[++ii]);
			continue;
		} else if (!strcmp(argv[ii], "-h")) {
			show_help();
			return 0;
		}
	}

	// show copyright message
	show_copyright();

	// initialize program variables.
	g.timetable = new int[g.threads];
	g.rbuffer = new char[g.total_size * g.threads];
	g.wbuffer = new char[g.total_size * g.threads];

	if (g.threads > 1) {
		pthread_t *threads; threads = new pthread_t[g.threads];
		for (int ii = 0; ii < g.threads; ii ++) {
			int *tid = new int; *tid = ii;
			pthread_create(&threads[ii], NULL, memtest, tid);
		}
		for (int ii = 0; ii < g.threads; ii ++) {
			pthread_join(threads[ii], NULL);
		}
	} else { // g.threads == 1
		memtest(NULL);
	}

	// show results
	show_results();

	return 0;
}
