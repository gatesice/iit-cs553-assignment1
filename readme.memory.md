## Compile

### Compile on Linux

`g++ -std=c++0x -lpthread memory.cpp -o memory_linux`

### Compile on Mac

`clang++ -std=c++11 -stdlib=libc++ -Weverything -lpthread memory.cpp -o memory_mac`

## Basic usage

assume filename is `memory_linux`.

`memory_linux [-threads NUM] [-loops NUM] [-blocksize NUM] [-totalsize NUM] [-seq|-rnd] [-h]`

## Help code

```
CS553 Assignment 1 - Memory Benchmarking

Arguments:

-blocksize: the size each time program read/write.
            byte|kb|mb|<number in bytes>
            byte=1, kb=1024, mb=1024*1024

            default: byte

-seq -sequential: run this program in sequential access.

-rnd -random: run this program in random access.

-loops: number of loops when random access, default=10000

-totalsize: total size of memory blocks, per thread.
            if you have 3 threads and 1000 total size, you will actually have 3 * 1000 = 3000 in total.

-threads: number of threads.
          If 1 is specified, program will not use pthread lib.
          default: 1

```

## Example Usage:

### Do memory test, default variables (sequential, byte blocksize, 100MB totalsize)

`./memory_linux`

### Test sequential access with 2 threads and 1 kb blocksize and 50MB size for each thread.

`./memory_linux -blocksize kb -totalsize 52428800 -threads 2`

### Do random test with 1000 loops with 1MB blocksize

`./memory_linux -rnd -loops 1000 -blocksize mb`
