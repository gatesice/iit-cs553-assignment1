#!/bin/sh

#$ -S /bin/sh
# This script automatically run disk benchmarking.

echo "CS553 Assignment 1 disk Benchmarking"
echo "    Designed by Lu Wang (A20315534)"

# sequential speed.
for block_size in byte kb mb
do
	echo Block_size=$block_size
	for threads in 1 2 4
	do
		for n in 1 2 3
		do
			echo attempt $n read test
			bin/disk_linux -seq -threads $threads -blocksize $block_size -read
		done
		for n in 1 2 3
		do
			echo attempt $n write test
			bin/disk_linux -seq -threads $threads -blocksize $block_size -write
		done
	done
done

# random speed
for block_size in byte kb mb
do
	echo Random access Block_size=$block_size
	for threads in 1 2 4
	do
		for n in 1 2 3
		do
			echo attempt $n
			bin/disk_linux -rnd -blocksize $block_size -threads $threads
		done
		for n in 1 2 3
		do
			echo attempt $n write test
			bin/disk_linux -rnd -threads $threads -blocksize $block_size -write
		done
	done
done
