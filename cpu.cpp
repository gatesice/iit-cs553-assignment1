// =====================================================
//
//   ass1_cpu.cpp
//
//   author: Gates_ice
//
//   CS553 Assignment 1
//   CPU Benchmarking section.
//
// =====================================================

#include <iostream>
#include <iomanip>
#include <chrono>
#include <cstring>

using namespace std;

// ### Global variables ### //
enum Mode { FLOAT, INT };

typedef struct {
	int loops;
	int mode;
	int threads;
	int *timetable;
} Global;

Global g;


// ### Thread functions ### //

/**
 * thread function.
 * mesure the float operations
 */
void *flops(void *args) {
	// get current tid. If there is 1 thread in all, tid is 0.
	int tid = 0;
	if (g.threads > 1) {
		tid = *((int*)args);
	}

	// begin time measurement.
	auto begin = chrono::high_resolution_clock::now();

	// main loop
	for (int ii = 0; ii < g.loops; ii ++);

	// end time measurement
	auto end = chrono::high_resolution_clock::now();
	auto dur = end - begin;

	double dd = 0.0;
	auto begin2 = chrono::high_resolution_clock::now();

	// main loop 2
	for (int ii = g.loops; ii > 0; ii --) {
		dd += 0.11;
		dd += 0.22;
		dd += 0.33;
		dd += 0.44;
		dd -= 1.10;
	}

	auto end2 = chrono::high_resolution_clock::now();
	dur = (end2 - begin2 - dur)/5;

	g.timetable[tid] = std::chrono::duration_cast<std::chrono::milliseconds>(dur).count();
}

/**
 * thread function
 * mesure the int operations
 */
void *iops(void *args) {
	// get current tid. If there is 1 thread in all, tid is 0.
	int tid = 0;
	if (g.threads > 1) {
		tid = *((int*)args);
	}

	// begin time measurement.
	auto begin = chrono::high_resolution_clock::now();

	// main loop
	for (int ii  = 0; ii < g.loops; ii ++);

	// end time measurement
	auto end = chrono::high_resolution_clock::now();
	auto dur = end - begin;

	// main loop 2

	int jj = 0;
	auto begin2 = chrono::high_resolution_clock::now();
	for (int ii = g.loops; ii > 0; ii --) {
		jj += 1;
		jj += 2;
		jj += 3;
		jj += 4;
		jj -= 10;
	}
	auto end2 = chrono::high_resolution_clock::now();
	dur = (end2 - begin2 - dur) / 5;
	g.timetable[tid] = std::chrono::duration_cast<std::chrono::milliseconds>(dur).count();
}


// ### Miscellious functions ### //

/**
 * show program help screen.
 */
void show_help() {
	cout << "CS553 Assignment 1 - CPU Benchmarking" << endl << endl

		<< "Arguments:" << endl << endl

		<< setw(8) << "-loops: number of loops does the benchmarking do." << endl
		<< setw(8) << "        default: 1000000000" << endl << endl

		<< setw(8) << "-float: do float benchmarking." << endl << endl

		<< setw(8) << "-int: do int benchmarking." << endl << endl

		<< setw(8) << "-threads: number of threads" << endl
		<< setw(8) << "          if you specify 1 thread, the program will not use pthread." << endl << endl

		<< setw(8) << "-h: show this screen." << endl << endl;
}

/**
 * show copyright message.
 */
void show_copyright() {
	cout << "CS553 Assignment 1 - CPU Benchmarking" << endl
		<< "    Author: Gates Wong (Lu Wang, A20315534)" << endl
		<< "    use `ass1_cpu -h` to see more helps." << endl;
}

/**
 * show results.
 */
void show_results() {
	     // `={12} RESULTS ={12}`
	cout << "============ RESULTS ============" << endl << endl

		// `Benchmarking mode: ((?:float)|(?:int))`
		<< "Benchmarking mode: " << (g.mode == Mode::FLOAT ? "float" : "int") << endl

		// `Number of threads: (\d+)`
		<< "Number of threads: " << g.threads << endl

		// `Number of loops per thread: (\d+)`
		<< "Number of loops per thread: " << g.loops << endl << endl;

	// List all time.
	for (int ii = 0; ii < g.threads; ii ++) {
		// `thread (\d+) takes (\d+) ms to run. (?:FLOPS)|(?:IOPS): (\d+(?:\.\d+)?)/ms.`
		cout << "thread " << ii << " takes " << g.timetable[ii] << " ms to run. "
			<< (g.mode == Mode::FLOAT ? "FLOPS: ": "IOPS: ")
			<< g.loops / g.timetable[ii] << "/ms." << endl;
	}
}


// ### main() ### //

int main(int argc, char* argv[]) {
	// default values
	g.loops = 1000000000;
	g.mode = Mode::FLOAT;
	g.threads = 1;

	// parse the CLI arguments.
	for (int ii = 0; ii < argc; ii ++) {
		if (!strcmp(argv[ii], "-loops")) {
			g.loops = atoi(argv[++ii]);
			continue;
		} else if (!strcmp(argv[ii], "-float")) {
			g.mode = Mode::FLOAT;
			continue;
		} else if (!strcmp(argv[ii], "-int")){
			g.mode = Mode::INT;
			continue;
		} else if (!strcmp(argv[ii], "-threads")) {
			g.threads = atoi(argv[++ii]);
		} else if (!strcmp(argv[ii], "-h")) {
			show_help();
			return 0;
		}
	}

	// show copyright message
	show_copyright();

	// initialize program variables.
	g.timetable = new int[g.threads];
	if (g.threads > 1) {
		pthread_t *threads; threads = new pthread_t[g.threads];
		for (int ii = 0; ii < g.threads; ii ++) {
			int *tid = new int; *tid = ii;
			if (Mode::FLOAT == g.mode) {
				pthread_create(&threads[ii], NULL, flops, tid);
			} else if (Mode::INT == g.mode) {
				pthread_create(&threads[ii], NULL, iops, tid);
			} else {
				cout << "ERROR! Invalid mode." << endl;
				return 1;
			}
		}
		for (int ii = 0; ii < g.threads; ii ++) {
			pthread_join(threads[ii], NULL);
		}
	} else { // g.threads == 1
		if (Mode::FLOAT == g.mode) {
			(*flops)(NULL);
		} else if (Mode::INT == g.mode) {
			(*iops)(NULL);
		} else {
			cout << "ERROR! Invalid mode." << endl;
			return 1;
		}
	}

	// show results
	show_results();

	return 0;
}
