#!/bin/sh
# This file is used for compile all codes

# cpu part:
g++ -lpthread -std=c++0x cpu.cpp -o bin/cpu_linux

# gpu part:
nvcc gpu.cu -o bin/gpu_linux

# memory part:
g++ -lpthread -std=c++0x memory.cpp -o bin/memory_linux

# disk part:


# network part:
