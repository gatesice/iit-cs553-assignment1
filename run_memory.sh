#!/bin/sh

#$ -S /bin/sh
# This script automatically run memory benchmarking.

echo "CS553 Assignment 1 memory Benchmarking"
echo "    Designed by Lu Wang (A20315534)"

# sequential speed.
for block_size in byte kb mb
do
	echo Block_size=$block_size
	for threads in 1 2
	do
		for n in 1 2 3
		do
			echo attempt $n
			bin/memory_linux -seq -threads $threads -blocksize $block_size
		done
	done
done

# random speed
for block_size in byte kb mb
do
	echo Random access Block_size=$block_size
	for threads in 1 2
	do
		for n in 1 2 3
		do
			echo attempt $n
			bin/memory_linux -rnd -blocksize $block_size -threads $threads
		done
	done
done
