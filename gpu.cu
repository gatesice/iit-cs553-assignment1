// =====================================================
//
//   ass1_gpu.cu
//
//   author: Gates_ice
//
//   CS553 Assignment 1
//   GPU Benchmarking section.
//
//   changelog:
//   - 2014-09-21: ported back to <ctime> library for time function, because current version
//                 nvcc in jarvis clusters (5.5) doesn't support c++0x standards. Will use
//                 time functions in CS546 codes.
//
// =====================================================

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <ctime>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/times.h>

#define MODE_FLOAT 	0x00
#define MODE_INT 	0x01

using namespace std;

typedef struct {
	int loops;
	int threads;
	int mode;
	float memtime;
	float gputime;
	int blocksize;
	bool memtest;
} Global;

Global g;

// returns a seed for srand based on the time
unsigned int time_seed() {
	struct timeval t;
	struct timezone tzdummy;

	gettimeofday(&t, &tzdummy);
	return (unsigned int)(t.tv_usec);
}

void check_err(cudaError_t err) {
	if (err != cudaSuccess) {
		fprintf(stderr, "%s in %s at line %d\n",
			cudaGetErrorString(err),
			__FILE__,
			__LINE__);
		exit(1);
	}
}

int* make_block() {
	int* m = new int[1024 * 1024 * g.blocksize / sizeof(int)];
	return m;
}

// device functions

__global__ void cuda_flops(int *loops, double *b) {
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	for (double a = 0; a < *loops; a += 1.0) {
		b[tid] += 0.1;
	}
}

__global__ void cuda_iops(int *loops, int *b) {
	int tid = blockIdx.x * blockDim.x + threadIdx.x;
	for (int a = 0; a < *loops; a ++) {
		b[tid] ++;
	}
}


// ### Miscellious functions ### //

/**
* show program help screen.
*/
void show_help() {
	cout << "CS553 Assignment 1 - GPU Benchmarking" << endl << endl

		<< "Arguments:" << endl << endl

		<< setw(8) << "-loops: number of loops does the benchmarking do." << endl
		<< setw(8) << "        default: 10000000" << endl << endl

		<< setw(8) << "-float: do float benchmarking." << endl << endl

		<< setw(8) << "-int: do int benchmarking." << endl << endl

		<< setw(8) << "-threads: number of threads" << endl << endl

		<< setw(8) << "-memtest: do memory test instead" << endl << endl

		<< setw(8) << "-blocksize: the size used in memory test (byte, kb, mb)" << endl << endl

		<< setw(8) << "-h: show this screen." << endl << endl;
}

/**
* show copyright message.
*/
void show_copyright() {
	cout << "CS553 Assignment 1 - GPU Benchmarking" << endl
		<< "    Author: Gates Wong (Lu Wang, A20315534)" << endl
		<< "    use `gpu -h` to see more helps." << endl;
}

/**
* show results.
*/
void show_results() {
		// `={12} RESULTS ={12}`
	cout << "============ RESULTS ============" << endl << endl;

	if (!g.memtest) {
		// `Benchmarking mode: ((?:float)|(?:int))`
		cout << "Benchmarking mode: " << (g.mode == MODE_FLOAT ? "float" : "int") << endl

		// `Number of threads: (\d+)`
			<< "Number of threads: " << g.threads << endl

			// `Number of loops per thread: (\d+)`
			<< "Number of loops per thread: " << g.loops << endl << endl

			<< "GPU kernel function running times: " << g.gputime / 5 << " ms." << endl << endl;
	} else {
		cout << "Blocksize: " << g.blocksize << endl << endl

			// ``
			<< "Memory copy 1000 times for " << g.blocksize << " block: " << g.memtime << " ms." << endl << endl;
	}


}


int main(int argc, char* argv[]) {
	g.loops = 10000000;
	g.threads = 1;
	g.mode = MODE_FLOAT;
	g.blocksize = 1;
	g.memtest = false;
	cudaError_t err;

	// parse the CLI arguments
	for (int ii = 0; ii < argc; ii ++) {
		if (!strcmp(argv[ii], "-loops")) {
			g.loops = atoi(argv[++ii]);
			continue;
		} else if (!strcmp(argv[ii], "-float")) {
			g.mode = MODE_FLOAT;
			continue;
		} else if (!strcmp(argv[ii], "-int")) {
			g.mode = MODE_INT;
			continue;
		} else if (!strcmp(argv[ii], "-threads")) {
			g.threads = atoi(argv[++ii]);
			continue;
		} else if (!strcmp(argv[ii], "-h")) {
			show_help();
			return 0;
		} else if (!strcmp(argv[ii], "-blocksize")) {
			char a[10]; strcpy(a, argv[++ii]);
			if (!strcmp(a, "byte")) {
				g.blocksize = 1;
			} else if (!strcmp(a, "kb")) {
				g.blocksize = 1024;
			} else if (!strcmp(a, "mb")) {
				g.blocksize = 1024 * 1024;
			} else { g.blocksize = 1; }
			continue;
		} else if (!strcmp(argv[ii], "-memtest")) {
			g.memtest = true;
			continue;
		}
	}

	/* Timing variables */
	struct timeval etstart, etstop;  /* Elapsed times using gettimeofday() */
	struct timezone tzdummy;
	clock_t etstart2, etstop2;  /* Elapsed times using times() */
	unsigned long long usecstart, usecstop;
	struct tms cputstart, cputstop;  /* CPU times for my processes */

	// show copyright message
	show_copyright();

	// initialize program variables.

	if (!g.memtest) {
		cout << "GPU kernel function started." << endl;
		int *gl, *g_l; g_l = new int; *g_l = g.loops;
		int *ib, *ib_l;
		double *db, *db_l;

		cudaMalloc((int **) &gl, sizeof(int));
		cudaMemcpy(gl, (void*)g_l, sizeof(int), cudaMemcpyHostToDevice);

		if (g.mode == MODE_INT) {
			ib_l = new int[g.threads];
			for (int ii = 0; ii < g.threads; ii ++) {
				ib_l[ii] = 0;
			}
			cudaMalloc((int **)&ib, sizeof(int) * g.threads);
			cudaMemcpy(ib, (void*)ib_l, sizeof(int) * g.threads, cudaMemcpyHostToDevice);
		} else if (g.mode == MODE_FLOAT) {
			db_l = new double[g.threads];
			for (int ii = 0; ii < g.threads; ii ++ ) {
				db_l[ii] = 0.1;
			}
			cudaMalloc((double **)&db, sizeof(double) * g.threads);
			cudaMemcpy(db, (void*)db_l, sizeof(double) * g.threads, cudaMemcpyHostToDevice);
		}


		cudaEvent_t begin, end;
		cudaEventCreate(&begin);
		cudaEventCreate(&end);

		cudaEventRecord(begin, 0);

		// kernel functions
		if (g.mode == MODE_FLOAT) { cuda_flops<<<1, g.threads>>> (gl, db); }
		else if (g.mode == MODE_INT) { cuda_iops<<<1, g.threads>>> (gl, ib); }
		else { return 1; }
		cudaThreadSynchronize();

		cudaEventRecord(end, 0);
		cudaEventSynchronize(end);
		cudaEventElapsedTime(&(g.gputime), begin, end);
		cudaEventDestroy(begin);
		cudaEventDestroy(end);

		cudaFree(gl);
	} else {
		// gpu memory copy test
		cout << "GPU memory started." << endl;
		int *H = make_block(), *D;	// make a 16 MB memory block.
									// H: host memory
									// D: device memory
		cudaMalloc((int **) &D, g.blocksize);

		/* Start Clock */
		gettimeofday(&etstart, &tzdummy);
		etstart2 = times(&cputstart);

		for (int i = 0; i < 1000; i ++) {
			cudaMemcpy(D, (void*)H, g.blocksize, cudaMemcpyHostToDevice);
		}

		/* Stop Clock */
		gettimeofday(&etstop, &tzdummy);
		etstop2 = times(&cputstop);
		usecstart = (unsigned long long)etstart.tv_sec * 1000000 + etstart.tv_usec;
		usecstop = (unsigned long long)etstop.tv_sec * 1000000 + etstop.tv_usec;
		g.memtime = (float)(usecstop - usecstart)/1000.0;

		cudaFree(D);
	}

	// show results
	show_results();


	return 0;
}
