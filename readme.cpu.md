## Compile

### Compile on Linux

`g++ -std=c++0x -lpthread cpu.cpp -o cpu_linux`

### Compile on Mac

`clang++ -std=c++11 -stdlib=libc++ -Weverything -lpthread cpu.cpp -o cpu_mac`

## Basic usage

assume filename is `cpu_linux`.

`cpu_linux [-threads NUM] [-loops NUM] [-int|-float] [-h]`

## Help code

```
CS553 Assignment 1 - CPU Benchmarking

Arguments:

-loops: number of loops does the benchmarking do.
        default: 1000000000

-float: do float benchmarking.

-int: do int benchmarking.

-threads: number of threads
          if you specify 1 thread, the program will not use pthread.

-h: show this screen.
```

## Example Usage:

### Do an IOPS test, test 1,000,000,000 loops:

`./cpu_linux -int -loops 1000000000`

### Do an FLOPS test, with 4 threads:

`./cpu_linux -float -threads 4`

### Do test with all default set

`./cpu_linux`

### Do test with IOPS test, 100,000,000 loops and 8 threads:

`./cpu_linux -int -loops 100000000 -threads 8`
