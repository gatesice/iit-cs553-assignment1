// =====================================================
//
//   benchmark_network_client.c
//
//   author: Jie Shen
//
//   CS553 Assignment 1.5
//   Network Benchmarking section.
//   2014-09-19
//   Acknowlege: Thanks for Gates Wang for his outstanding
//   programming skills
// =====================================================



#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define BENCHMARKING_SERVER_PORT       8888
#define FILE_NAME_MAX_SIZE             512
#define rcv_file_name                 "client_test_data"

#define LOCAL_IP		                  "127.0.0.1"

int     port = 5678;
int     to_port = 1234;
char    to_ip[] = "127.0.0.1";
char    startcmd[13] = "StartTransfer";
char    quitcmd[12] = "QuitTransfer";
int 	g_receive_size = 0;
int MAXLEN = 1;

typedef struct {
  char 	*protocal;
  int 	buffer_size;
  int 	threads;
  int   server_port;
  char 	*info;
} Global;

Global g_parameter;
typedef struct {
  int from_port;
  int to_port;
  int port_number;
} Local;
Local l1_parameter,l2_parameter,l_parameter1,l_parameter2;


void * function_client_udp(void * argv)
{

  struct sockaddr_in local_sin;
  struct sockaddr_in to_sin;
  int    mysocket;
	pthread_t self_id;
	self_id=pthread_self();
printf("Hello My name is    from thread %u \n",(unsigned int)self_id);
  FILE   *fp;
  //char   buf[MAXLEN];
  int     len;
 int i;
 int j = 10000;
  int x,y;
  Local a = *((Local*)argv);
  x=a.from_port;
  y=a.to_port;
  printf("the from port is : %d \n ", x);
  printf("the to port is : %d \n ", y);

  char * buf=NULL;
  buf=(char*)malloc(sizeof(char));
  int recvlen = sizeof(struct sockaddr_in),sendlen = sizeof(struct sockaddr_in);
  bzero( &local_sin, sizeof(local_sin));
  local_sin.sin_family = AF_INET;
  local_sin.sin_addr.s_addr = inet_addr("127.0.0.1");
  local_sin.sin_port = htons( x );
  bzero( &to_sin, sizeof(to_sin));
  to_sin.sin_family = AF_INET;
  to_sin.sin_addr.s_addr = inet_addr("127.0.0.1");
  to_sin.sin_port = htons(y);
  mysocket = socket( AF_INET, SOCK_DGRAM, 0 );
  bind( mysocket, ( struct sockaddr * )&local_sin, sizeof(local_sin) );
  i = sendto(mysocket,startcmd,13,0,(struct sockaddr *)&to_sin,sendlen);
  if(i == -1){
    perror("sendto error");
  exit(-1);
  }
  printf("I am here !\n");
/*
  while(len=recvfrom(mysocket,buf,40,0,(struct sockaddr *)&to_sin,&recvlen))
  {
      if(strncmp(buf,quitcmd,12)==0)
        {
  //        fclose(fp);
          break;
        }
     //   fwrite(buf,len,1,fp);
  }
  printf("the len is %d!\n",len);
*/
 while (j>=0)
 {
	 len=recvfrom(mysocket,buf,MAXLEN,0,(struct sockaddr *)&to_sin,&recvlen);
 	 g_receive_size=g_receive_size+len;
 	   printf("the len is %d!\n",len);
 	   printf("the g_receive_size is %d!\n",g_receive_size);
 	   j--;
 }
  close(mysocket);
  printf("Quit_UDP_Client!!\n");
  printf("Hello This is  from thread %u \n",(unsigned int)self_id);
  return;

}


void * function_client_tcp(void* arg)
{
  struct sockaddr_in client_addr;
  bzero(&client_addr, sizeof(client_addr));
  client_addr.sin_family = AF_INET; // internet
  client_addr.sin_addr.s_addr = htons(INADDR_ANY); // INADDR_ANY
  client_addr.sin_port = htons(0); // auto allocated,
  int buffer_size;  
  //buffer_size= g_parameter.buffer_size;
  buffer_size = 1;
  Local local_parameter = *((Local*)arg);
  int port_number = local_parameter.port_number;
  printf ("the portname is %d \n",port_number);
  int client_socket = socket(AF_INET, SOCK_STREAM, 0);
  if (client_socket < 0)
  {
      printf("Create Socket Failed!\n");
      exit(1);
  }
  // bind ip and port
  if (bind(client_socket, (struct sockaddr*)&client_addr, sizeof(client_addr)))
  {
      printf("Client Bind Port Failed!\n");
      exit(1);
  }
  struct sockaddr_in  server_addr;
  bzero(&server_addr, sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  if (inet_aton(LOCAL_IP, &server_addr.sin_addr) == 0)
  {
      printf("Server IP Address Error!\n");
      exit(1);
  }
  server_addr.sin_port = htons(BENCHMARKING_SERVER_PORT);
  socklen_t server_addr_length = sizeof(server_addr);
  if (connect(client_socket, (struct sockaddr*)&server_addr, server_addr_length) < 0)
  {
      printf("Can Not Connect To the LOCALHOST!Please try again!\n");
      exit(1);
  }
 
  char buffer[buffer_size];
  bzero(buffer, sizeof(buffer));


  send(client_socket, buffer, buffer_size, 0);

  bzero(buffer, sizeof(buffer));
  int length = 0;
  printf("The buffersizie   is %d  \n", buffer_size);
  while(length = recv(client_socket, buffer, buffer_size, 0))
  {
	  g_receive_size=g_receive_size+length;
	  printf("The length is %d  \n", g_receive_size);
      if (length < 0)
      {
          printf("Recieve Data Finished!\n");          
          break;
      }
      bzero(buffer, buffer_size);
  }
    close(client_socket);
}


int main(int argc, char **argv)
{
    g_parameter.buffer_size = 1048576;
    g_parameter.threads = 1;
    g_parameter.protocal =   (char*) malloc (strlen("TCP")+1);
    g_parameter.server_port = BENCHMARKING_SERVER_PORT ;
    int port_number,port_number2;
    clock_t start, finish;
    double duration;
    double bandwidth;
      printf("Please input TCP|UDP,thread !\n");
    if (argc != 3 )
    {
      printf("Please input TCP|UDP,thread and try again!\n");
      exit(1);
    }
    strcpy(g_parameter.protocal,"TCP");
    if (strcmp(argv[1],"TCP")==0)
      {strcpy(g_parameter.protocal,"TCP");}
    else if (strcmp(argv[1],"UDP")==0)
      {strcpy(g_parameter.protocal,"UDP");}
    else
    {
      printf("TCP/UDP not input,Please input TCP or UDP and try again! \n");
      return 1;
    }
    if (strcmp(argv[2],"1")==0)
      {g_parameter.threads = 1;}
    else if (strcmp(argv[2],"2")==0)
    {
        g_parameter.threads = 2;
    }
    else
    {
        printf("Thread not input,Please input Thread and try again! \n");
        return 1;
    }
    
    start = clock();
    if (strcmp(argv[1],"TCP")==0)
    {
        if (1==g_parameter.threads)
          {

             l_parameter1.port_number=BENCHMARKING_SERVER_PORT;
            (*function_client_tcp)(&l_parameter1);
          }
          else if (2==g_parameter.threads)
          {
             pthread_t threads[2];
             port_number=BENCHMARKING_SERVER_PORT;
             pthread_create(&threads[0], NULL, function_client_tcp, &l_parameter1 );
             port_number2= 8898;  // another port
             pthread_create(&threads[1], NULL, function_client_tcp, &l_parameter2 );
             pthread_join(threads[0], NULL);
             pthread_join(threads[1], NULL);
          }
          else
          {
              printf("Thread number entered error!Please enter 1 or 2! Others are invalid!\n");
              return 1;
          }
     }
     else if (strcmp(argv[1],"UDP")==0)
    {
			l1_parameter.from_port=5678;
            l1_parameter.to_port=1234;
          if (1==g_parameter.threads)
          {
			  printf("Single Thread go ... !\n");
           
            (*function_client_udp)(&l1_parameter);
          }
          else if (2==g_parameter.threads)
          {
			  printf("Double Thread  go ....!\n");
            pthread_t threads[2];
            pthread_create(&threads[0], NULL, function_client_udp, &l1_parameter );
            l2_parameter.from_port=5688;
            l2_parameter.to_port=1244;
            pthread_create(&threads[1], NULL, function_client_udp, &l2_parameter );
            pthread_join(threads[0], NULL);
            pthread_join(threads[1], NULL);
          }
          else
          {
              printf("Thread number entered error!Please enter 1 or 2! Others are invalid!\n");
              return 0;
          }
    }
    else
      {
        return 0;
      }
      finish = clock();
      //printf("Recieve File:\t %s From Server[%s] Finished!\n", file_name, argv[1]);
      duration = (double)(finish - start) / CLOCKS_PER_SEC;
      bandwidth=(g_receive_size / duration)/ (1024 * 1024);
      printf( "%f seconds\n", duration );
      
      printf( "The latency is         %f   second \n", bandwidth );
   
      
      
    //  finished，close socket
    //  fclose(fp);
    return 0;
}
