#!/bin/sh

#$ -S /bin/sh
# This script automatically run GPU benchmarking.

echo "CS553 Assignment 1 GPU Benchmarking"
echo "    Designed by Lu Wang (A20315534)"

# int operations test, get IOPS
for num_threads in 1 8 32 128 256 512
do
	for n in 1 2 3
	do
		echo attempt $n
		bin/gpu_linux -int -threads $num_threads -loops 100000000
	done
done

# float operations test, get FLOPS
for num_threads in 1 8 32 128 256 512
do
	for n in 1 2 3
	do
		echo attempt $n
		bin/gpu_linux -float -threads $num_threads -loops 100000000
	done
done

# memory test
for mem_blocksize in byte kb mb
do
	for n in 1 2 3
	do
		echo attempt $n
		bin/gpu_linux -blocksize $mem_blocksize -memtest
	done
done
