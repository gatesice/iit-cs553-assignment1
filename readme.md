### How to compile C++11 on MAC OS?

`clang++ -std=c++11 -stdlib=libc++ -Weverything main.cpp`

Reference [this thread](http://stackoverflow.com/questions/14228856/how-to-compile-c-with-c11-support-in-mac-terminal).

### How to compile cpu.cpp on MAC

`clang++ -std=c++11 -stdlib=libc++ -Weverything -lpthread cpu.cpp -o cpu_mac`

This will produce an executable file named `cpu_mac`. Run with `-h` for detailed running info.

### How to compile cpu.cpp on Linux machine.

`g++ -std=c++0x -lpthread cpu.cpp -o cpu_linux`

### How to compile gpu.cu on Jarvis nodes.

`nvcc gpu.cu -o gpu_linux`

### change log: `gpu.cu` ported back to non-c++0x standard.

2014-09-21: ported back to <ctime> library for time function,
because current version in jarvis clusters (5.5) doesn't support c++0x standards.
Will ues time functions in CS546 codes.
