	// =====================================================
//
//   benchmark_network_server.c
//
//   author: Jie Shen
//
//   CS553 Assignment 1.5
//   Network Benchmarking section.
//   2014-09-19
//   Acknowlege: Thanks for Gates Wang for his outstanding
//   programming skills
// =====================================================

#include "stdio.h"
#include "stdlib.h"
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#define BENCHMARKING_SERVER_PORT   8888
#define LENGTH_OF_LISTEN_QUEUE     20
#define FILE_NAME_MAX_SIZE         512
#define PAGE_SIZE  1048576
#define FINISH_FLAG    "FILE_TRANSPORT_FINISH"
#define SENDING_BUFFER 4096
#define MAXLEN 65507
char    startcmd[13] = "StartTransfer";
char    quitcmd[12] = "QuitTransfer";
char    to_ip[] = "127.0.0.1";
int     port = 1234;
int     to_port = 5678;


//Global is used to save global parameter such as TCP/UDP,threads
typedef struct {
	char 	*protocal;
	int 	buffer_size;
	int 	threads;
	int   server_port;
	char 	*info;
	int  latency;
} Global;

Global g_parameter;

typedef struct {
  int from_port;
  int to_port;
  int port_number;
} Local;
Local l1_parameter,l2_parameter,l_parameter1,l_parameter2;


void * function_server_udp(void* args)
{
		struct sockaddr_in local_sin;
		struct sockaddr_in to_sin;
		int port_number = *((int*)args);
		int    mysock;
		FILE *fp;
		int filelen;
		char * buf=NULL;
		int buffer_size;
		buffer_size= g_parameter.buffer_size;
		buf=(char*)malloc(65535*sizeof(char));
		int len;
		int i;
		int x,y;
		Local a = *((Local*)args);
		x=a.from_port;
		y=a.to_port;
		printf("the from port is : %d \n ", x);
		printf("the to port is : %d \n ", y);
		printf("I am here1 !\n");
		int    recvlen = sizeof(struct sockaddr_in),sendlen = sizeof(struct sockaddr_in);
		bzero( &local_sin, sizeof(local_sin));
		local_sin.sin_family = AF_INET;
		local_sin.sin_addr.s_addr = inet_addr("127.0.0.1");
		local_sin.sin_port = htons( x );
		bzero( &to_sin, sizeof(to_sin));
		to_sin.sin_family = AF_INET;
		to_sin.sin_addr.s_addr = inet_addr(to_ip);
		to_sin.sin_port = htons( y);
		mysock = socket( AF_INET, SOCK_DGRAM, 0 ); // create socket
		int bind_stats = bind( mysock, ( struct sockaddr * )&local_sin, sizeof(local_sin) );   //bind
		if(bind_stats == -1){
		perror("bind error");
		exit(-1);
		}
		len=recvfrom(mysock,buf,MAXLEN,0,(struct sockaddr *)&to_sin,&recvlen);
		printf("len is %d\n",len);
		if(len == -1){
			perror("recvfrom  return error");
			exit(-1);
		}
		if(strncmp(buf,startcmd,13)==0) //begin_transver.
		{
				if((fp=fopen("/tmp/benchmark_network.dat","w+"))==NULL)
				{
						printf("Testing file open failure!!\n");
						exit(-1);
				}
				fseek(fp,65535,SEEK_END);
				filelen = ftell(fp);
					printf( "filelen = %d\n",filelen);
						printf( "len is %d \n" ,len);
				while ( 1 )
				{
						bzero(buf,MAXLEN);
						fread(buf,MAXLEN,1,fp);
						printf( "filelen = %d\n",filelen);
						printf( "len is %d \n" ,len);
						len = sendto(mysock,buf,MAXLEN,0,(struct sockaddr *)&to_sin,sendlen);

						if(len < 0)
						{

								printf( "len is %d   \n" ,len);
								exit(1);
						}
						else
						{
								sendto(mysock,buf,filelen,0,(struct sockaddr *)&to_sin,sendlen);
								if(len < 0)
								{
										printf("send  2 failure!!\n");
										break;
								}
							sendto(mysock,quitcmd,strlen(quitcmd),0,(struct sockaddr *)&to_sin,sendlen);

						}
				}
		}

		printf("Quiting_UDP_Services !\n");
		return ;
}



void * function_server_tcp(void* args)
{
	    FILE *fh;
	    int buffer_size;
			buffer_size = g_parameter.buffer_size;
			int port_number = *((int*)args);
			printf("port_number    is :%d \n",port_number);
	    struct sockaddr_in   server_addr;
    	bzero(&server_addr, sizeof(server_addr));
    	server_addr.sin_family = AF_INET;
    	server_addr.sin_addr.s_addr = htons(INADDR_ANY);
   	  server_addr.sin_port = htons(port_number);
    	int server_socket = socket(PF_INET, SOCK_STREAM, 0);
      printf("Server Listener Started........................\n");
			printf("\n");
    	if (server_socket < 0)
    	{
       		printf("Creating Socket Failed!\n");
        	exit(1);    	}
    	//  binding socket and ip
    	if (bind(server_socket, (struct sockaddr*)&server_addr, sizeof(server_addr)))
    	{
        	printf("Server Binding Port: %d Failed!\n", port_number);
        	exit(1);
    	}
    	// server_socket for listener
    	if (listen(server_socket, LENGTH_OF_LISTEN_QUEUE))
    	{
        	printf("Server Listener Failed!\n");
        	exit(1);
    	}
    	// server client is a dead loop
    	while(1)
    	{
       		struct sockaddr_in client_addr;
        	socklen_t   length = sizeof(client_addr);
        	int new_server_socket = accept(server_socket, (struct sockaddr*)&client_addr, &length);
        	if (new_server_socket < 0)
        	{
        		printf("Server Accept Failed!\n");
        		break;
        	}
        	char buffer[buffer_size];
        	bzero(buffer, sizeof(buffer));
        	length = recv(new_server_socket, buffer, buffer_size, 0);
        	if (length < 0)
        	{
        		printf("Server Recieve Data Failed!\n");
        		break;
        	}
        	char file_name[FILE_NAME_MAX_SIZE + 1];
        	bzero(file_name, sizeof(file_name));
        	strncpy(file_name, buffer, strlen(buffer) > FILE_NAME_MAX_SIZE ? FILE_NAME_MAX_SIZE : strlen(buffer));
        	FILE *fp = fopen("/tmp/benchmark_network.dat", "r");
			//		printf("File NAME IS %s Not Found!\n", file_name);
        	if (fp == NULL)
        	{
        		printf("File:\t%s Not Found!\n", file_name);
        	}
        	else
        	{
        		bzero(buffer, buffer_size);
        		int file_block_length = 0;
        		while( (file_block_length = fread(buffer, sizeof(char), buffer_size, fp)) > 0)
        		{
                		printf("file_block_length = %d\n", file_block_length);

                	if (send(new_server_socket, buffer, file_block_length, 0) < 0)
                	{
                		printf("Send File:\t%s Failed!\n", file_name);
                		break;
                	}
                	bzero(buffer, sizeof(buffer));
            	}
            		fclose(fp);
            		printf("File:\t%s Transfer Finished!\n", file_name);
       		}
        	close(new_server_socket);
    	}
    	close(server_socket);
      return ;
}


void show_parameter()
{
	int i;
	i = g_parameter.buffer_size;
	printf("buffer_size is :%d \n",i);
	printf("threads     is :%d \n",g_parameter.threads);
  printf("Protocal    is :%s \n",g_parameter.protocal);
	return ;
}

int init_test_file()
{
	FILE * fh;
	int i;
 	int sz = sizeof(char)*PAGE_SIZE;
  char* buf = (char*)malloc(sz);
	fh= fopen("/tmp/benchmark_network.dat","wb");
	if(fh == NULL)
	{
		printf("Creating file ERROR!");
		printf("\n");
		return 0;
	}
	else
	{
		printf("Creating file size of 1M .................  Done!\n");
	  fwrite (buf , sizeof(char), sz, fh);
		fclose(fh);
		return 1;
	}
}

void show_about()
{
	printf("==================Version 2014-09-17=====================\n");
	printf("==================== Jie Shen ===========================\n");
	printf("====================A20325456 ===========================\n");
}

int main(int argc, char **argv)
{
	g_parameter.buffer_size = 1048576;
	g_parameter.threads = 1;
  g_parameter.protocal =   (char*) malloc (strlen("TCP")+1);
	g_parameter.server_port = BENCHMARKING_SERVER_PORT ;
	int port_number,port_number2;
  if (g_parameter.protocal==NULL) exit (1);
  if (argc != 4 )
  {
    printf("Please input TCP|UDP,thread,buffersize and try again!\n");
    exit(1);
  }
	strcpy(g_parameter.protocal,"TCP");
	if (strcmp(argv[1],"TCP")==0)
    {strcpy(g_parameter.protocal,"TCP");}
  else if (strcmp(argv[1],"UDP")==0)
    {strcpy(g_parameter.protocal,"UDP");}
  else
  {
    printf("TCP/UDP not input,Please input TCP or UDP and try again! \n");
    return 1;
  }
	if (strcmp(argv[2],"1")==0)
		{g_parameter.threads = 1;}
	else if (strcmp(argv[2],"2")==0)
	{
			g_parameter.threads = 2;
	}
	else
	{
			printf("Thread not input,Please input Thread 1 or 2 and try again! \n");
			return 1;
	}
	if (strcmp(argv[3],"1")==0)
		{g_parameter.buffer_size = 1;}
	else if (strcmp(argv[3],"4096")==0)
	{
			g_parameter.buffer_size =4096;
	}
	else if(strcmp(argv[3],"65536")==0)
	{
			g_parameter.buffer_size =65536;
	}
	else
	{
		printf("Please input Buffer Size 1,4096 or 65536,Others are in invalid! \n");
		return 1;
	}
  show_parameter();
	show_about();
	init_test_file();
	if (strcmp(argv[1],"TCP")==0)
	{
			if (1==g_parameter.threads)
				{
		 			port_number=BENCHMARKING_SERVER_PORT;
	   	    (*function_server_tcp)(&port_number);
				}
				else if (2==g_parameter.threads)
				{
     			pthread_t threads[2];
		 		  port_number=BENCHMARKING_SERVER_PORT;
       		pthread_create(&threads[0], NULL, function_server_tcp, &port_number );
		 		  port_number2= 8898;  // another port
          pthread_create(&threads[1], NULL, function_server_tcp, &port_number2 );
          pthread_join(threads[0], NULL);
          pthread_join(threads[1], NULL);
				}
				else
					{
						printf("Thread number entered error!Please enter 1 or 2! Others are invalid!\n");
					}
  			return 0;
 	}
 else if (strcmp(argv[1],"UDP")==0)
  {
	  		l1_parameter.from_port=1234;
            l1_parameter.to_port=5678;

			if (1==g_parameter.threads)
				{
					port_number=port;
					(*function_server_udp)(&l1_parameter);
				}
				else if (2==g_parameter.threads)
				{
					pthread_t threads[2];

					pthread_create(&threads[0], NULL, function_server_udp, &l1_parameter );
					l2_parameter.from_port=1244;
					l2_parameter.to_port=5688;
					pthread_create(&threads[1], NULL, function_server_udp, &l2_parameter );
					pthread_join(threads[0], NULL);
					pthread_join(threads[1], NULL);
				}
				else
					{
						printf("Thread number entered error!Please enter 1 or 2! Others are invalid!\n");
					}
					return 0;
	}
 else
	return 0;
}
